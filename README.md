# OpenML dataset: rmftsa_ladata

https://www.openml.org/d/666

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Data Sets for 'Regression Models for Time Series Analysis' by
B. Kedem and K. Fokianos, Wiley 2002. Submitted by Kostas
Fokianos (fokianos@ucy.ac.cy) [8/Nov/02] (176k)

Note: - attribute names were generated manually
- information about data taken from here:
http://lib.stat.cmu.edu/datasets/

File: ../data/rmftsa/ladata

LA Pollution-Mortality Study:
1970-1979, 508 observations,  6-day spacing. Weekly FILTERED data.
The data were lowpass filtered, filtering out frequencies above 0.1
cycles per day.
Mortality:          (1) Mrt: Total Mortality
(2) Rsp: Respiratory Mortality
(3) Crd: Cardiovascular Mortality
Weather:            (4) Tmp: Temperature
(5) Hum: Relative Humidity
Pollution:          (6) Crb: Carbon Monoxide
(7) Slf: Sulfur Dioxideglm.LAshumway
(8) Nit: Nitrogen Dioxide
(9) Hdr: Hydrocarbons
(10) Ozn: Ozone
(11) Par: Particulates


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/666) of an [OpenML dataset](https://www.openml.org/d/666). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/666/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/666/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/666/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

